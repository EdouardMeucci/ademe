/*
File structure :
------------------
1 - Open & Close Filter
------------------
2 - Scroll detection
------------------
*/

// 1 - Open & Close menu (for mobile devices)
function toggleFilter() {
    document.getElementById("filter-content").classList.toggle("filter-open");
    document.getElementById("archive-buttons").classList.toggle("filter-open");
}

// 2 - Scroll detection : #topbar gets sticky on scroll
var menu = document.getElementById("topbar");

window.onscroll = function() {
    if (window.pageYOffset > 0) {
        menu.classList.add("scroll");
    } else {
        menu.classList.remove("scroll");
    }
};