<?php
/**
 * Template part for displaying search result in search.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 */

?>

<?php $ami_color = get_field('ami', ihag_get_term($post, "ami")); ?>

<article class="search-card">

	<!-- Title -->
	<a class="link-color btm-padding-tiny <?php echo ihag_ami_color_class($ami_color, 'color1'); ?>" href="<?php the_permalink();?>" >
		<h2 class="h3-like no-margin"><?php the_title();?></h2>
	</a>

	<!-- Excerpt -->
	<?php the_excerpt();?>

	<a class="button-brd" href="<?php the_permalink();?>" title="<?php the_title();?>">
		<p class="no-margin"><?php _e('Lire', 'ademe'); ?></p>
	</a>

</article>
