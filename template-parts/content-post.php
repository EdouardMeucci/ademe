<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 */
?>

<?php 

// $pt = get_post_type_object( get_post_type() );
// $pt_label = $pt->labels->singular_name;

$ami_color = get_field('ami', ihag_get_term($post, "ami"));

$ami = ihag_get_term($post, 'ami') ;

// Post title
echo '<header class="top-padding-regular">';

	echo '<div class="wrapper bg-banner bg-banner-page-title">';
		// echo '<i class="h1-like wrapper-medium left-for-desktop is-centered">'.  $pt_label  .'</i>';
		echo '<p class="h1-like wrapper-medium left-for-desktop is-centered">Actualités</p>';
		echo '<h1 class="h2-like wrapper-medium left-for-desktop is-centered '. ihag_ami_color_class($ami_color, 'color1') .' ">'. get_the_title() .'</h1>';

		// Post Date
		echo '<time class="body-like wrapper-medium is-centered uppercase" datetime="'. get_the_date('Y-m-d') .'">';
			echo date_i18n( get_option('date_format'), strtotime(get_the_date('Ymd')));
		echo '</time>';

		// Post ami
		if ($ami) {
			echo '<div class="wrapper-medium is-centered top-padding-tiny"><i class="body-like button-like-brd uppercase inline">'. $ami->name .'</i></div>';
		}

		// Post Thumbnail
		if ( has_post_thumbnail() ) {
			echo '<div class="wrapper-small center is-centered thumbnail-container top-padding-tiny">';
				the_post_thumbnail( 'wrapper-large-half', ['class' => 'img-responsive']);
				if (get_the_post_thumbnail_caption()) {
					echo '<i class="italic left">'. get_the_post_thumbnail_caption() .'</i>';
				}
			echo '</div>';
		}
	echo '</div>';
echo '</header>';

// Share
get_template_part( 'template-parts/part','share' );

// Post content
if ( get_the_content() ) {
	echo '<main id="raw-content" class="above-bg-banner bg-banner-security">';
	the_content();
	echo '</main>';
} else {
	get_template_part( 'template-parts/content', 'none' );
}
?>

<script>
document.addEventListener('DOMContentLoaded', function() {
    var els = document.getElementsByClassName("menu-item-actu");
    
    Array.prototype.forEach.call(els, function(el) {
	   el.classList.add('current-menu-item');
	});
});
</script>