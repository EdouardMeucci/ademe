<?php

echo '<div class="wrapper above-bg-banner bg-banner-security">';
    echo '<i class="wrapper-medium left-for-desktop is-centered body-like">';

    if ( (is_archive()) || is_page_template('templates/tpl-archive-post.php') || is_page_template('templates/tpl-archive-ressource.php') || is_search() ) {
        _e("Pas de contenu lié à votre sélection.", "ademe");
    }

    echo '</i>';
echo '</div>';

?>
