<?php
/**
 * Block Name: Bloc présentation ADEME
 */
 ?>

<section class="wp-block blk-about btm-padding-regular wrapper center bg-banner">

<?php
$text = get_field('text');

if ( !$text ):

	echo '<em>Renseigner le contenu</em>';
	
else :

	// Title
	$title = get_field('title');
	if( $title ) {
		echo '<h2 class="wrapper-medium is-centered left-for-desktop">'. $title .'</h2>';
	}

	// Content Part 1 (intro : 1 column)
	if( $text ) {
		echo '<div class="wrapper-medium is-centered btm-padding-small left entry-content">'. $text .'</div>';
	}

	// Content Part 2 (list : up to  3 columns)
	if( have_rows('vocations') ):

		echo '<ol class="is-centered center">';

		while( have_rows('vocations') ) : the_row();

			if( get_sub_field('content') ):
				echo '<li class="left"><div class="entry-content">'. get_sub_field('content') .'</div></li>';
			endif;

		endwhile;

		echo '</ol>';

	endif;	

	// Link
	$link = get_field('link');
	if( $link ): 
		$link_url = $link['url'];
		$link_title = $link['title'];
		$link_target = $link['target'] ? $link['target'] : '_self';

		// Fallback if there is no $link_title
		if (empty($link_title)) {
			$link_title = $link_url;
		}
		?>

		<a class="button" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>" title="<?php echo esc_html( $link_title ); ?>">
			<?php echo esc_html( $link_title );?>
		</a>

	<?php endif; ?>
	
<?php endif; ?>
</section>
