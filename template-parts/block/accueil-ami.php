<?php
/**
 * Block Name: Bloc Accueil AMI
 */
 ?>

<!-- Bloc Accueil AMI -->
<section class="blk-sub-menu wp-block wrapper bg-banner btm-padding-regular">

<?php

if ( !get_field('amis') ):

	echo '<em>Renseigner le contenu</em>';

else :

	$title = get_field('title');
	if ($title) {
		echo '<h2 class="center wrapper-medium is-centered margin-b">'.$title.'</h2>';
	}

	if( have_rows('amis') ):

		echo '<nav class="wrapper-large is-centered center">';

		while( have_rows('amis') ) : the_row();

			$ami = get_sub_field('ami');

			switch_to_blog($ami);
			echo '<a class="blk-submenu-item link-color '.ihag_ami_color_class($ami, 'color1').'" href="'. home_url() .'">';
			restore_current_blog();

				$size = 'block-sub-menu';
				$image = get_sub_field('image'); 
				?>

				<span class="image-container">
					<?php ihag_get_attachment_image($image, $size, $attr = array( "class" => "img-in-link" ));?>
				</span>

				<?php

				echo '<p class="h3-like">';
					switch_to_blog($ami);
				 	echo get_bloginfo('name');
					restore_current_blog();
				echo '</p>';

			echo '</a>';

		endwhile;

		echo '</nav>';

	endif;
	
endif; 
?>

</section>