<?php
/**
 * Template part for displaying content-ressource in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 */
?>

<?php

$pt = get_post_type_object( get_post_type() );
$pt_label = $pt->labels->singular_name;
$ami = ihag_get_term($post, 'ami') ;
$ami_color = get_field('ami',$ami);
$ami_id = $ami->term_id;

// Ressource title
echo '<header class="top-padding-regular">';

	echo '<div class="wrapper bg-banner bg-banner-page-title">';
		echo '<i class="h1-like wrapper-medium left-for-desktop is-centered">'.  $pt_label  .'</i>';
		echo '<h1 class="h2-like wrapper-medium left-for-desktop is-centered '. ihag_ami_color_class($ami_color, 'color1') .'">'. get_the_title() .'</h1>';

		// Ressource subtitle
		if(get_field('sub-head')) {
			echo '<h2 class="h3-like wrapper-medium left-for-desktop is-centered '. ihag_ami_color_class($ami_color, 'color2') .' ">'. get_field('sub-head') .'</h2>';
		}
		// Ressource ami
		if ($ami) {
			echo '<div class="wrapper-medium btm-padding-tiny left-for-desktop is-centered"><i class="body-like button-like-brd uppercase inline">'. $ami->name .'</i></div>';
		}

		// Ressource Thumbnail
		if ( has_post_thumbnail() ) {
			echo '<div class="wrapper-small center is-centered thumbnail-container top-padding-tiny">';
				the_post_thumbnail( 'wrapper-large-half');
				if (get_the_post_thumbnail_caption()) {
					echo '<i class="italic left">'. get_the_post_thumbnail_caption() .'</i>';
				}
			echo '</div>';
		}
	echo '</div>';
echo '</header>';

// Share
get_template_part( 'template-parts/part','share' );

// Ressource content
if ( get_the_content() ) {
	echo '<main id="raw-content" class="above-bg-banner bg-banner-security">';
	the_content();
	echo '</main>';
} else {
	get_template_part( 'template-parts/content', 'none' );
}


// Ressource pagination
$prev_post = get_previous_post();
$next_post = get_next_post();

if ( (!empty( $prev_post )) || (!empty( $next_post )) ):

	echo '<nav class="wrapper is-centered">';

		echo '<div id="ressource-nav"  class="wrapper-large is-centered '; if (empty( $prev_post )) { echo 'right';} echo '">';

			if (!empty( $prev_post )): 
				echo '<a class="left link-black v-padding-tiny" href="'.get_permalink( $prev_post->ID ).'">';
					echo '<img src="'. get_template_directory_uri().'/image/arrow-left.svg" alt="'; _e("Précédent", "ademe"); echo '" width="16" height="16">';
					echo '<i class="body-like uppercase h6-like no-margin">'; _e("Ressource précédente", "ademe"); echo '</i>';
					echo '<p class="no-margin">'. $prev_post->post_title .'</p>';
				echo '</a>';
			endif; 

			if (!empty( $next_post )): 
				echo '<a class="right link-black v-padding-tiny" href="'.get_permalink( $next_post->ID ).'">';
					echo '<img src="'. get_template_directory_uri().'/image/arrow-right.svg" alt="'; _e("Suivant", "ademe"); echo '" width="16" height="16">';
					echo '<i class="body-like uppercase h6-like no-margin">'; _e("Ressource suivante", "ademe"); echo '</i>';
					echo '<p class="no-margin">'. $next_post->post_title .'</p>';
				echo '</a>';
			endif; 

		echo '</DIV>';

	echo '</nav>';

endif; 

?>
