<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 */

get_header();
?>

<?php global $wp_query; ?>

<?php
	echo '<header class="top-padding-regular">';

	echo '<div class="wrapper bg-banner bg-banner-page-title">';
		echo '<i class="h1-like wrapper-medium left-for-desktop is-centered">'; _e( "Recherche", "ademe" ); echo '</i>';
		echo '<h1 class="h2-like wrapper-medium left-for-desktop is-centered">'; _e( "Résultat(s) pour : ", "ademe" ); echo get_search_query() .'</h1>';
		echo '<p class="wrapper-medium left-for-desktop is-centered">'. $wp_query->found_posts; _e( " résultat(s) trouvé(s) ", "ademe" ); echo '</p>';
	echo '</div>';

	echo '</header>';

	if ( have_posts() ) : 

	echo '<main class="wrapper above-bg-banner">';
		echo '<div class="wrapper-medium is-centered listing-search">';

			while ( have_posts() ) :
				$wp_query->the_post();
				get_template_part( 'template-parts/search' );
			endwhile;

			// Pagination
			ihag_page_navi();

		echo '</div>';        
	echo '</main>';

	else :

		get_template_part( 'template-parts/content', 'none' );

	endif;
?>

<?php
get_footer();
