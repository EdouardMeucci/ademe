if (document.forms.namedItem("contactForm")) {
    document.forms.namedItem("contactForm").addEventListener('submit', function(e) {
        document.getElementById('sendMessage').disabled = true; //limit le problème des doubles clic
        e.preventDefault();

        var form = document.forms.namedItem("contactForm");
        var formData = new FormData(form); //fonction native qui mets tout dans un format prêt a etre envoyé
        xhr = new XMLHttpRequest(); //prepare la requete
        xhr.open('POST', resturl + 'contactForm', true);
        xhr.onload = function() {
            if (xhr.status === 200) {
                document.getElementById('sendMessage').disabled = false;
                document.getElementById('sendMessage').classList.add("hiddenSubmitButton");
                document.getElementById('ResponseMessage').classList.add("showResponseMessage");
            }
        };
        xhr.send(formData);
    });
}



// if (document.forms.namedItem("footer-newslettter-form")) {
//     document.forms.namedItem("footer-newslettter-form").addEventListener('submit', function(e) {
//         document.getElementById('sendMessageNewsletter').disabled = true; //limit le problème des doubles clic
//         e.preventDefault();

//         var form = document.forms.namedItem("footer-newslettter-form");
//         var formData = new FormData(form);
//         xhr = new XMLHttpRequest();
//         xhr.open('POST', resturl + 'formNewsletter', true);
//         xhr.onload = function() {
//             if (xhr.status === 200) {
//                 document.getElementById('sendMessageNewsletter').disabled = false;
//                 document.getElementById('sendMessageNewsletter').classList.add("hiddenSubmitButton");
//                 document.getElementById('ResponseMessageNewsletter').innerText = xhr.response.replace('"', '').replace('"', '');
//                 document.getElementById('ResponseMessageNewsletter').classList.add("showResponseMessage");
//             }
//         };
//         xhr.send(formData);
//     });
// }
/*
File structure :
------------------
1 - Open & Close Filter
------------------
2 - Scroll detection
------------------
*/

// 1 - Open & Close menu (for mobile devices)
function toggleFilter() {
    document.getElementById("filter-content").classList.toggle("filter-open");
    document.getElementById("archive-buttons").classList.toggle("filter-open");
}

// 2 - Scroll detection : #topbar gets sticky on scroll
var menu = document.getElementById("topbar");

window.onscroll = function() {
    if (window.pageYOffset > 0) {
        menu.classList.add("scroll");
    } else {
        menu.classList.remove("scroll");
    }
};

/*
File structure :
------------------
1 - Open & Close menu
------------------
2 - Scroll detection
------------------
*/

// 1 - Open & Close menu (for mobile devices)
function toggleMenu() {
    document.getElementById("menu").classList.toggle("menu-open");
}

// 2 - Scroll detection : #topbar gets sticky on scroll
var menu = document.getElementById("topbar");

window.onscroll = function() {
    if (window.pageYOffset > 0) {
        menu.classList.add("scroll");
    } else {
        menu.classList.remove("scroll");
    }
};
document.addEventListener('DOMContentLoaded', function() {
    var els = document.getElementsByClassName("btn-modale");
    Array.prototype.forEach.call(els, function(el) {
        el.addEventListener("click", function(e) {
            e.preventDefault();
            e.stopPropagation();

            var modale = document.createElement("div");
            modale.classList.add("modale");
            modale.id = el.dataset.uniqId;

            var button = document.createElement("button");
            button.classList.add("closeModale");
            button.onclick = function() { document.getElementById(el.dataset.uniqId).remove(); };
            button.appendChild(document.createTextNode("X"));
            modale.appendChild(button);

            var modaleContent = document.createElement("div");
            modaleContent.id = 'modaleContent' + el.dataset.uniqId;
            modaleContent.classList.add("modaleContent");
            modale.appendChild(modaleContent);

            document.body.appendChild(modale);

            for (var key in iframe) {
                // check if the property/key is defined in the object itself, not in parent
                if (iframe.hasOwnProperty(key)) {
                    if (key == el.dataset.uniqId) {
                        document.getElementById('modaleContent' + el.dataset.uniqId).innerHTML = iframe[key];
                        modale.classList.add("active");
                    }
                }
            }
        });
    });
});
// organise - infinit scroll 
if (document.querySelector('#infinite-list')) {
    var base_url = window.location.href;
    var elm = document.querySelector('#infinite-list');
    // loader = document.querySelector('#loaderPost');
    var page = elm.dataset.page;
    var height = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight;
    load = false;
    ticking = false;
    window.addEventListener('scroll', function(e) {
        if (!ticking) {
            window.setTimeout(function() {
                ticking = false;
                //Do something
                if (!load && (elm.offsetTop + elm.clientHeight) < (window.scrollY + height)) { //scroll > bas de infinite-list
                    //inserer les éléments suivants
                    if (page >= elm.dataset.nbPageMax) {
                        console.log(load);
                    } else {
                        load = true;
                        readMorePost();
                    }

                }
            }, 300); //fréquence du scroll
        }
        ticking = true;
    });
}



function readMorePost() {
    page++;
    // loader.classList.add("active");
    var formData = new FormData();
    formData.append("page", page);
    formData.append("cpt", elm.dataset.cpt);
    formData.append("taxo", elm.dataset.taxo);
    formData.append("taxo_tag_var", elm.dataset.taxo_tag);

    console.log("readMorePost");

    xhr = new XMLHttpRequest();
    xhr.open('POST', resturl + 'scroll', true);
    xhr.onload = function() {
        if (xhr.status === 200) {
            load = false;
            console.log(xhr.response);
            elm.insertAdjacentHTML('beforeend', xhr.response);
            // window.history.replaceState("", "", elm.dataset.url + "page/" + page + "/");
            // loader.classList.remove("active");
        }
    };
    xhr.send(formData);
}



document.addEventListener('DOMContentLoaded', function() {
    var els = document.getElementsByClassName("custom-checkbox-input");
    Array.prototype.forEach.call(els, function(el) {
        el.addEventListener("change", function(e) {
            document.forms.namedItem("archive-filter").submit();
        });
    });
});
document.addEventListener('DOMContentLoaded', function() {
    var els = document.getElementsByClassName("JSrslink");
    var heightScreen = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight;
    var widthScreen = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
    Array.prototype.forEach.call(els, function(el) {
        el.addEventListener("click", function(e) {
            e.stopPropagation();
            e.preventDefault();
            var width = 320,
                height = 400,
                left = (widthScreen - width) / 2,
                top = (heightScreen - height) / 2,
                url = this.href,
                opts = 'status=1' +
                ',width=' + width +
                ',height=' + height +
                ',top=' + top +
                ',left=' + left;
            window.open(url, 'myWindow', opts);
            return false;
        });
    });

    /*
    copyLink = document.getElementsByClassName('copyLink');
    Array.prototype.forEach.call(copyLink, function(el) {
        el.addEventListener("click", function(e) {
            e.stopPropagation();
            copyLinkValue = el.getElementsByClassName('copyLinkValue');
            copyLinkValue[0].focus();
            copyLinkValue[0].select();
            document.execCommand("copy");
            el.classList.add('active');
        });

    });
    */



});