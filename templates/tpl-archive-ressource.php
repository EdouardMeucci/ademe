<?php
/*
Template Name: Ressource
*/
?>

<?php get_header(); ?>

<?php 
// Page title
echo '<header class="top-padding-regular">';

	echo '<div class="wrapper bg-banner bg-banner-page-title">';
		echo '<i class="h1-like wrapper-medium left-for-desktop is-centered">'. get_bloginfo('name') .'</i>';
		echo '<h1 class="h2-like wrapper-medium left-for-desktop is-centered">'. get_the_title() .'</h1>';
	echo '</div>';

echo '</header>';


/* Page content - not shown
if ( have_posts() ) : while (have_posts()) : the_post();
if ( get_the_content() ) {
	echo '<main id="raw-content" class="above-bg-banner bg-banner-security">';
	the_content();
	echo '</main>';
}
endwhile; endif;
*/

// Archive Ressource
// Query Settngs
$num_page = (get_query_var("paged") ? get_query_var("paged") : 1);
$type = "ressource";
$args = array(
	'paged' => $num_page,
	'post_type'   => $type,
	'tax_query' => array(
		'relation' => 'AND',
	)
);

$my_taxonomies = get_object_taxonomies('ressource');
foreach($my_taxonomies as $my_taxonomy){
	$taxonomy = get_taxonomy($my_taxonomy);
	if(isset($_GET[$taxonomy->name])){
		$args['tax_query'][] = array(
			'taxonomy' => $taxonomy->name,
			'field'    => 'slug',
			'terms'    => $_GET[$taxonomy->name],
		);
	}
}
	
query_posts($args);
global $wp_query; 

// Archive Content
echo '<main id="archive-content" class="wrapper above-bg-banner btm-padding-regular">';

	// Load Filters
	get_template_part( 'template-parts/part','taxo' ); 

	// Listing container
	echo '<div id="archive-listing">';


	if ( have_posts() ) : 

		echo '<div class="listing-ressource">';

			while (have_posts()) : the_post();
				get_template_part('template-parts/archive', "ressource");
			endwhile;

		echo '</div>';

		// Pagination
		ihag_page_navi();
		wp_reset_query();

	else:
	
		get_template_part( 'template-parts/content', 'none' );
	
	endif;

	echo '</div>';

echo '</main>';
?>

<?php 
get_footer();