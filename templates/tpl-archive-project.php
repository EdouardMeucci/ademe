<?php
/*
Template Name: Map
*/
?>

<?php get_header(); ?>
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

<header class="top-padding-regular">

	<!-- Titre -->
	<div class="wrapper bg-banner bg-banner-page-title">
		<i class="h1-like wrapper-medium left-for-desktop is-centered"><?php echo get_bloginfo('name'); ?></i>
		<h1 class="h2-like wrapper-medium left-for-desktop is-centered"><?php the_title(); ?></h1>
		<p>
			Utiliser le zoom pour afficher des projets hors métropole.
		</p>
	</div>

</header>

<main id="archive-project" class="above-bg-banner"> 

	<?php get_template_part( 'template-parts/part','taxo' ); ?>

	<div id="map"></div>
	<?php
	$tab_posts = array();
	$args = array( 	'posts_per_page' => -1,
					'post_type' => 'project',
					'tax_query' => array(
						'relation' => 'OR',
					)
				);
	$my_taxonomies = get_object_taxonomies('post');
	foreach($my_taxonomies as $my_taxonomy){
		$taxonomy = get_taxonomy($my_taxonomy);
		if(isset($_GET[$taxonomy->name])){
			$args['tax_query'][] = array(
				'taxonomy' => $taxonomy->name,
				'field'    => 'slug',
				'terms'    => $_GET[$taxonomy->name],
			);
		}
	}
	
	$myposts = get_posts( $args );
	$tab_posts = array();

	foreach ( $myposts as $post ) : setup_postdata( $post );

		$location = get_field('coordonnees');

		$term = get_the_terms($post, 'ami');
		$picto_url = get_field('picto_map', $term[0]);

		// $taxo_id = $taxo[0]->term_id;
	
		/*
		$tab_posts[get_the_title()] = array(
				"lat" => $location['lat'],
				"lon" => $location['lng'],
				"ctn" => '<h4>'.get_the_title().'</h4><p>'.get_the_content().'</p><a href="'.get_field('link').'" target="_blank">'.__('Voir le projet','ademe').'</a>',
			);
		*/

		if ( get_field('link') ) {	

			$tab_posts[get_the_title()] = array(
				"lat" => $location['lat'],
				"lon" => $location['lng'],
				"ctn" => '<h4>'.get_the_title().'</h4><p>'.get_the_content().'</p><a href="'.get_field('link').'" target="_blank">'.__('Voir le projet','ademe').'</a>',
				"picto" => $picto_url
			);
			//var_dump($tab_posts);

		} else {

			$tab_posts[get_the_title()] = array(
				"lat" => $location['lat'],
				"lon" => $location['lng'],
				"ctn" => '<h4>'.get_the_title().'</h4><p>'.get_the_content().'</p>',
				"picto" => $picto_url
			);
			
		}

		//'<div><h4>'.get_the_title().'</h4><p>'.get_the_content().'</p><a href="'.get_field('link').'" target="_blank">'.__('Voir le projet','ademe').'</a>';
		?>
	<?php endforeach;
	wp_reset_postdata();
	?>

	<script>
		var villes = <?php echo json_encode($tab_posts);?>;
		// On initialise la latitude et la longitude de Paris (centre de la carte)
		var lat = 46.22763;
		var lon = 2.21374;
		var macarte = null;


		// Fonction d'initialisation de la carte
		function initMap() {
			// Créer l'objet "macarte" et l'insèrer dans l'élément HTML qui a l'ID "map"
			macarte = L.map('map').setView([lat, lon], 5);
			// Leaflet ne récupère pas les cartes (tiles) sur un serveur par défaut. Nous devons lui préciser où nous souhaitons les récupérer. Ici, openstreetmap.fr
			L.tileLayer('https://{s}.tile.openstreetmap.fr/osmfr/{z}/{x}/{y}.png', {
			//L.tileLayer('https://cartodb-basemaps-{s}.global.ssl.fastly.net/light_all/{z}/{x}/{y}.png', {
				// Il est toujours bien de laisser le lien vers la source des données
				attribution: 'données © OpenStreetMap/ODbL - rendu OSM France',
				minZoom: 1,
				maxZoom: 20
			}).addTo(macarte);
			// Nous parcourons la liste des villes
			for (ville in villes) {
				var myIcon = L.icon({
					iconUrl: villes[ville].picto,
					iconSize: [22, 30],
					iconAnchor: [11, 30],
					popupAnchor: [0, -32],
				});

				var marker = L.marker([villes[ville].lat, villes[ville].lon], { icon: myIcon }).addTo(macarte);
				marker.bindPopup(villes[ville].ctn);
			}               	
		}
		window.onload = function() {
			// Fonction d'initialisation qui s'exécute lorsque le DOM est chargé
			initMap();
		};
	</script>

</main>

<!-- End of the loop -->
<?php endwhile; endif;?>

<?php get_footer(); ?>

