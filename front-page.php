<?php get_header(); ?>
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

<main> 

	<?php 
	echo '<div id="raw-content">';	
	the_content();
	echo '</div>';
	?>

	<!-- Actuality -->
	<section class="wrapper center bg-banner btm-padding-regular">

		<?php 
		echo '<h2 class="left-for-desktop wrapper-medium is-centered margin-b">'. __("Actualités", "ademe") .'</h2>';
		?>

		<div class="listing-post-home is-centered left wrapper-large">
		<?php
			// Condition import article
			$posts = get_posts(array(
				'post_type'			=> 'post',
				'post_status'		=> 'publish',
				'posts_per_page' 	=> 4,
			));
			
			// Import Article
			foreach ($posts as $post):
				setup_postdata( $post );
				get_template_part('template-parts/archive', 'post');
			endforeach;
			wp_reset_postdata();
		?>
		</div>

		<a href="<?php the_permalink( get_field('archive_post', 'option'));?>#start-listing" class="button is-centered">
			Plus d'actualités
		</a>
		

	</section>

	<!-- Event -->

	<section class="wrapper center bg-banner btm-padding-regular">

		<?php 
		echo '<h2 class="left-for-desktop wrapper-medium is-centered margin-b">'. __("À l'agenda", "ademe") .'</h2>';
		?>

		<div class="listing-event is-centered left wrapper-large">
			<!-- Condition import article -->
			<?php 
			$posts = get_posts( array(
				'post_type'			=> 'event',
				'post_status'		=> 'publish',
				'posts_per_page' 	=> 4,
				'order'				=> 'ASC',
				'orderby' 			=> 'meta_value_num',
				'meta_query' => array(
					/*
					array(
						'key' => 'date-start',
						'value' => date('Ymd'),
						'type' => 'DATE',
						'compare' => '>='
					)
					*/
				),
			));
				
			// Import Article
			if( $posts ):
				foreach ($posts as $post):
					setup_postdata( $post );
					get_template_part('template-parts/archive', 'event');
				endforeach;
				wp_reset_postdata();
			endif;
			?>
		</div>
		<a href="<?php the_permalink( get_field('archive_event', 'option'));?>" class="button is-centered">
			Plus d'évènements
		</a>
		
	</section>

</main>

<!-- End of the loop -->
<?php endwhile; endif;?>

<?php get_footer(); ?>

