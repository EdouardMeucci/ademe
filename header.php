<?php
/**
 * The header for our theme
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 */
?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="icon" type="image/png" href="<?php echo get_template_directory_uri(); ?>/favicon/favicon-32px.png" sizes="32x32">
	<link rel="icon" type="image/png" href="<?php echo get_template_directory_uri(); ?>/favicon/favicon-124px.png" sizes="124x124">
	<link rel="icon" type="image/png" href="<?php echo get_template_directory_uri(); ?>/favicon/favicon-124px.png" sizes="512x512">
	<link rel="icon" type="image/svg" href="<?php echo get_template_directory_uri(); ?>/favicon/favicon.svg">
	<link rel="apple-touch-icon" type="image/png" href="<?php echo get_template_directory_uri(); ?>/favicon/favicon-124px.png" />
	<link rel="manifest" href="<?php echo get_template_directory_uri(); ?>/favicon/manifest.json">
	<meta name="theme-color" content="#ffffff">
	<?php wp_head(); ?>

	<style>

	<?php

	if ( function_exists( 'wp_get_sites' ) ) {

	$sites = get_sites();

		if ($sites) {
			// Custom colors settings
			foreach ( $sites as $site ) {
				
				if($site->blog_id > 1){
					switch_to_blog($site->blog_id);

					echo "/* Custom colors for ami".$site->blog_id." */";
					
					if (get_field('color1', 'option')) {
						echo '.'.ihag_ami_color_class($site->blog_id, 'color1'). ' { color : '.get_field('color2', 'option').'; } ';
					} else {
						echo '.'.ihag_ami_color_class($site->blog_id, 'color1'). ' { color : #10A4B5; } ';
					}

					if (get_field('color2', 'option')) {
						echo '.'.ihag_ami_color_class($site->blog_id, 'color2'). ' { color : '.get_field('color3', 'option').'; } ';
					} else {
						echo '.'.ihag_ami_color_class($site->blog_id, 'color2'). ' { color : #148E83; } ';
					}

					if (get_field('color3', 'option')) {
						echo '.'.ihag_ami_color_class($site->blog_id, 'color3'). ' { color : '.get_field('color4', 'option').'; } ';
					} else {
						echo '.'.ihag_ami_color_class($site->blog_id, 'color3'). ' { color : #06796E; } ';
					}

					if (get_field('color4', 'option')) {
						echo '.'.ihag_ami_color_class($site->blog_id, 'color4'). ' { color : '.get_field('color4', 'option').'; } ';
					} else {
						echo '.'.ihag_ami_color_class($site->blog_id, 'color4'). ' { color : #03645A; } ';
					}
				}
				restore_current_blog();
			}
		}
	} 
	?>
	</style>
	<script type="text/javascript" src="https://tarteaucitron.io/load.js?domain=experimentationsurbaines.ademe.fr&uuid=2df1000bf385655312abede8e30421664e7d594e"></script>
</head>


<body <?php body_class(); ?>>

	<!-- Skip Links-->
	<a class="skip-link" tabindex="0"  href="#content"><?php esc_html_e( 'Accéder au contenu', 'ademe' ); ?></a> 
	<a class="skip-link" tabindex="0" href="#menu"><?php esc_html_e( 'Menu', 'ademe' ); ?></a>
	<a class="skip-link" tabindex="0"  href="#form-search"><?php esc_html_e( 'Faire une recherche', 'ademe' ); ?></a>
	
	<nav id="topbar" class="h-padding-regular">
	
		<!-- Logo -->
		<a id="topbar-logo" class="link-discrete" href="<?php echo get_home_url(); ?>" title="<?php esc_html_e( 'Lien vers la page d\'accueil', 'ademe') ?>">
			<?php 
			//Custom Logos 
			$logo1 = get_field('logo1_header', 'options');
			$logo2 = get_field('logo2_header', 'options');
			if( $logo1 ) { 
				echo wp_get_attachment_image( $logo1, $size, false,);?>
			<?php 
			}
			if( $logo2) { 
				echo wp_get_attachment_image( $logo2, $size, false,);
			}
			?>
			<p><?php echo get_bloginfo('name'); ?></p>
		</a>

		<!-- Burger button -->
		<button id="burger-menu" class="link-icon has-border desktop-hidden huge-hidden" onclick="toggleMenu()" aria-label="<?php esc_html_e( 'Ouvrir le menu', 'ademe' ); ?>">
			<img aria-hidden="true" src="<?php echo get_stylesheet_directory_uri(); ?>/image/burger.svg" height="18" width="24">
		</button>

		<!-- Menu -->
		<div id="menu">

			<!-- Close Button -->
			<button id="close-menu" class="link-icon has-border icon-left desktop-hidden huge-hidden" onclick="toggleMenu()" aria-label="<?php esc_html_e( 'Fermer le menu', 'ademe' ); ?>">
				<img aria-hidden="true" src="<?php echo get_stylesheet_directory_uri(); ?>/image/cross.svg" height="20" width="30">
				<?php _e('Fermer', 'ademe');?>
			</button>

			<!-- Search -->		
			<form id="form-search" action="<?php echo home_url();?>" method="get" class="white-menu">
				<label for="search"><?php _e('Rechercher', 'ademe');?></label>
				<input type="text" name="s" id="search" placeholder="<?php _e("Tapez votre recherche ici", "digitemis");?>" value="<?php the_search_query(); ?>"/>
				<input type="image" class="link-icon has-border" id="search-image" alt="<?php echo esc_attr_x( 'Search', 'submit button' ); ?>" src="<?php echo get_template_directory_uri(); ?>/image/search.svg"  height="40" width="40">
			</form>

			<!-- Réseaux sociaux -->
			<ul id="header-social">
				<?php 
				$facebook = get_field('facebook', 'option');
				$twitter = get_field('twitter', 'option');
				$linkedin = get_field('linkedin', 'option');
				$youtube = get_field('youtube', 'option');

				if( $facebook ): 
					$link = $facebook;
					$link_url = $link['url'];
					$link_title = $link['title'];
					$link_target = $link['target'] ? $link['target'] : '_self';
					?>
					<li>
						<a class="link-icon" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>" title="<?php echo esc_html( $link_title ); ?>">
							<img aria-hidden="true" src="<?php echo get_template_directory_uri(); ?>/image/facebook.svg" height="22" width="22">
						</a>
					</li>
				<?php 
				endif;

				if( $twitter ): 
					$link = $twitter;
					$link_url = $link['url'];
					$link_title = $link['title'];
					$link_target = $link['target'] ? $link['target'] : '_self';
					?>
					<li>
						<a class="link-icon" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>" title="<?php echo esc_html( $link_title ); ?>">
							<img aria-hidden="true" src="<?php echo get_template_directory_uri(); ?>/image/twitter.svg" height="22" width="22">
						</a>
					</li>
				<?php 
				endif; 
				
				if( $linkedin ): 
					$link = $linkedin;
					$link_url = $link['url'];
					$link_title = $link['title'];
					$link_target = $link['target'] ? $link['target'] : '_self';
					?>
					<li>
						<a class="link-icon" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>" title="<?php echo esc_html( $link_title ); ?>">
							<img aria-hidden="true" src="<?php echo get_template_directory_uri(); ?>/image/linkedin.svg" height="22" width="22">
						</a>
					</li>
				<?php 
				endif;

				if( $youtube ): 
					$link = $youtube;
					$link_url = $link['url'];
					$link_title = $link['title'];
					$link_target = $link['target'] ? $link['target'] : '_self';
					?>
					<li>
						<a class="link-icon" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>" title="<?php echo esc_html( $link_title ); ?>">
							<img aria-hidden="true" src="<?php echo get_template_directory_uri(); ?>/image/youtube.svg" height="22" width="22">
						</a>
					<li>
				<?php endif; ?>
			</ul>

			<!-- Links -->
			<?php echo ihag_menu('primary'); ?>

		</div>

	</nav>

	<!-- #content -->
	<div id="content">
	